<?php

declare(strict_types=1);

namespace FlyingAnvil\AudioTool\Wave;

use FlyingAnvil\AudioTool\DataObject\WaveHeader;
use FlyingAnvil\Libfa\DataObject\Math\Physic\Amplitude;
use FlyingAnvil\Libfa\DataObject\Math\Physic\Frequency;
use FlyingAnvil\Libfa\Math\Algebra\SineWave;
use FlyingAnvil\Libfa\Wrapper\File;

class WaveFile
{
    private array $samples;

    public function __construct(
        private WaveHeader $header,
    ) {}

    public function setSamples(array $samples): void
    {
        $this->samples = $samples;
    }

    public function write(File $file): void
    {
        $head = $this->header->buildStringValue();

        $file->open('wb');
        $file->write($head);
        $file->write('data____');

        $tau = 6.28318530718;
        $maxVolume = 2 ** ($this->header->getBitsPerSample() - 1) - 8;
        $sampleRate = $this->header->getSampleRate();

        $frequency = Frequency::create(261.626);
        $duration  = 3;

        $sampleCountPerChannel = $sampleRate * $duration;

        $amplitude = Amplitude::create($maxVolume);
        $sine      = new SineWave($frequency, $amplitude);
        for ($i = 0; $i < $sampleCountPerChannel; $i++) {
            $seconds  = $i / $sampleRate;
            $progress = $i / $sampleCountPerChannel;

            $value = $sine->evaluate($tau * $seconds);

            if ($i <= 10) {
                echo 'value: ' . $value . ' | ' . 'seconds: ' . $tau * $seconds;
                echo PHP_EOL;
            }

            $channel1 = $value * $progress;
            $channel2 = $value * (1 - $progress);

            $file->writeUInt16LittleEndian((int)$channel1);
            $file->writeUInt16LittleEndian((int)$channel2);
        }

        $dataChunkPosition = strlen($head);
        $fileSize = $file->tell();

        // Fix Chunk Header
        $file->seek($dataChunkPosition + 4);
        $file->writeUInt32LittleEndian($fileSize - $dataChunkPosition + 8);

        // Fix File Header
        $file->seek(4);
        $file->writeUInt32LittleEndian($fileSize - 8);
    }
}
