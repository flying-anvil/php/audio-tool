<?php

declare(strict_types=1);

namespace FlyingAnvil\AudioTool\DataObject;

use FlyingAnvil\Fileinfo\Conversion\StringValue;
use FlyingAnvil\Fileinfo\DataObject\DataObject;
use FlyingAnvil\Libfa\Wrapper\File;
use Stringable;

class WaveHeader implements DataObject, StringValue, Stringable
{
    private const CHUNK_ID     = 'RIFF';
    private const FORMAT       = 'WAVE';
    private const SUB_CHUNK_ID = 'fmt ';

    private function __construct(
        private int $channelCount,
        private int $sampleRate,
        private int $bitsPerSample,
    ) {}

    public static function create(
        int $channelCount,
        int $sampleRate,
        int $bitsPerSample,
    ): self {
        return new self(
            $channelCount,
            $sampleRate,
            $bitsPerSample,
        );
    }

    public function buildStringValue(): string
    {
        $head = File::loadWrapper('php://', 'memory');
        $head->open('wb+');

        $head->write(self::CHUNK_ID . '____' . self::FORMAT . self::SUB_CHUNK_ID);
        $head->writeUInt32LittleEndian(16); // Subchunk Size (?)
        $head->writeUInt16LittleEndian(1);  // PCM interger samples (?)
        $head->writeUInt16LittleEndian($this->channelCount);
        $head->writeUInt32LittleEndian($this->sampleRate);
        $head->writeUInt32LittleEndian($this->calculateByteRate());
        $head->writeUInt16LittleEndian($this->calculateBlockAlign());
        $head->writeUInt16LittleEndian($this->bitsPerSample);

        $head->rewind();
        return $head->read(64);
    }

    public function calculateByteRate(): int
    {
        return (int)($this->sampleRate * $this->channelCount * .125);
    }

    private function calculateBlockAlign(): int
    {
        return (int)($this->channelCount * $this->bitsPerSample * .125);
    }

    public function getChannelCount(): int
    {
        return $this->channelCount;
    }

    public function getSampleRate(): int
    {
        return $this->sampleRate;
    }

    public function getBitsPerSample(): int
    {
        return $this->bitsPerSample;
    }

    public function __toString(): string
    {
        return $this->buildStringValue();
    }

    public function toString(): string
    {
        return $this->buildStringValue();
    }

    public function jsonSerialize(): array
    {
        return [
            'PCM'           => 1,
            'channelCount'  => $this->channelCount,
            'sampleRate'    => $this->sampleRate,
            'byteRate'      => $this->calculateByteRate(),
            'dataBlockSize' => 4,
            'bitsPerSample' => $this->bitsPerSample,
        ];
    }
}
