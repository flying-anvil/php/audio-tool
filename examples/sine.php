<?php

use FlyingAnvil\AudioTool\DataObject\WaveHeader;
use FlyingAnvil\AudioTool\Wave\WaveFile;
use FlyingAnvil\Libfa\Wrapper\File;

require_once __DIR__ . '/../vendor/autoload.php';

$wave = new WaveFile(WaveHeader::create(
    2, // stereo
    44100,
    16, // 16 bit sample quality
));

$wave->write(File::load(__DIR__ . '/output/sine.wav'));
